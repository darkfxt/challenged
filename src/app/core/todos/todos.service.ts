import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ITodo } from '../entities/todo.interface';

@Injectable({
  providedIn: 'root',
})
export class TodosService {
  private todos: Array<ITodo> = [];

  constructor() { }

  public addTodo(todo: ITodo): void {
    this.todos.push({...todo, id: this.todos.length});
  }

  public checkTodo(id: number): void {
    const todoToCheck = this.todos.find((t) => t.id === id);
    this.todos.splice(id, 1, todoToCheck);
  }

  public getAll(): Observable<Array<ITodo>> {
    return of(this.todos);
  }

  public getCompletedTodos(): Observable<Array<ITodo>> {
    return of(this.todos.filter((todo: ITodo) => todo.completed));
  }
}
