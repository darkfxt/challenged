import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'todo-app',
    pathMatch: 'full',
  },
  {
    path: 'todo-app',
    loadChildren: () =>
      import('./features/todo-container/todo-container-routing.module').then((r) => r.TodoContainerRoutingModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
