import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';
import { TodosService } from '../../core/todos/todos.service';
import { TodoContainerMaterialModule } from './todo-container-material.module';

import { TodoContainerRoutingModule } from './todo-container-routing.module';
import { TodoContainerComponent } from './todo-container.component';
import { TodoItemComponent } from './todo-list/todo-item/todo-item.component';
import { TodoListComponent } from './todo-list/todo-list.component';


@NgModule({
  declarations: [TodoContainerComponent, TodoListComponent, TodoItemComponent],
  imports: [
    CommonModule,
    TodoContainerRoutingModule,
    FormsModule,
    TodoContainerMaterialModule,
  ],
  providers: [
    TodosService,
  ],
})
export class TodoContainerModule { }
