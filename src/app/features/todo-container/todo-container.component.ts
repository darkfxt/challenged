import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ITodo } from '../../core/entities/todo.interface';
import { TodosService } from '../../core/todos/todos.service';

@Component({
  selector: 'app-todo-container',
  templateUrl: './todo-container.component.html',
  styles: [],
})
export class TodoContainerComponent implements OnInit {

  public todos$: Observable<Array<ITodo>>;
  public newTodo: string;

  constructor(
    private todosService: TodosService,
  ) { }

  public ngOnInit(): void {
    this.loadData();
  }

  public addTodo(): void {
    this.todosService.addTodo({ title: this.newTodo });
    this.newTodo = '';
  }

  public checkTodo(todo: ITodo): void{
    this.todosService.checkTodo(todo.id);
  }

  private loadData() {
    this.todos$ = this.todosService.getAll();
  }

}
