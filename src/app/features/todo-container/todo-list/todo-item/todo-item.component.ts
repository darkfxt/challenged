import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ITodo } from '../../../../core/entities/todo.interface';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss'],
})
export class TodoItemComponent {

  @Input() public todo: ITodo;

  @Output() public todoChecked: EventEmitter<ITodo> = new EventEmitter<ITodo>();

  constructor() { }

  public todoCheckClicked(todo: ITodo) {
    this.todoChecked.emit(todo);
  }

}
