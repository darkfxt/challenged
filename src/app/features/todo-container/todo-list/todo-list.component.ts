import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ITodo } from '../../../core/entities/todo.interface';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent implements OnInit {

  @Input() public todos: Array<ITodo>;

  @Output() public todoModified: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  public ngOnInit(): void {
  }

  public todoChecked(todo: ITodo) {
    this.todoModified.emit(todo);
  }

}
